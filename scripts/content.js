chrome.runtime.sendMessage({command: "register"});
chrome.runtime.onMessage.addListener(function (message) {
	function click_button(cls){
		let btn = document.getElementsByClassName(cls);
		if(btn.length > 0){
			btn[0].click();
		}
  }

  let likeStatus = document.getElementById('like-button-renderer');
	switch (message.command) {
		case "next-track":
			click_button('next-button');
			break;
		case "previous-track":
			click_button('previous-button');
			break;
		case "play-pause":
			click_button('play-pause-button');
			break;
		case "like-track":
      if(likeStatus.getAttribute('like-status') !== 'LIKE') {
        click_button('like');
      }
			break;
		case "dislike-track":
			click_button('dislike');
			break;
	}
});